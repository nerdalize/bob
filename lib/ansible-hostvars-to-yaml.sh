#!/bin/bash
#
#
# get host parameters from ansible inventory file, and put them
# in a yaml dict indexed by ansible_hostname (first word on the line)
#

[ -z $1 ] && echo "ERROR: $0 needs an input file" && exit 1


echo -e "#\n# This yaml is generated from the vagrant-generated ansible inventory. \n#\n"
cat "$1" | grep -v "^\s*#" | grep ansible_ssh_host | \
	# replace first (non greedy) space with ': '
	sed 's/ /: /' | \
	# use remaining spaces as indentation
	sed 's/ /\n    /g' | \
	# replace = with :
	sed 's/=/: /g'

