#!/usr/bin/env python

import sys, yaml, json

def die(s):
  print("Error: %s" % s)
  sys.exit(1)

def main():
  if len(sys.argv)<2:
    input = "/dev/stdin"
  else:
    input = sys.argv[1]
  with open (input, "r") as yml_file:
    y = yml_file.read()
    json.dump(yaml.load(y), sys.stdout, indent=4)


if __name__=="__main__":
  main()

