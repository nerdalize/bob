#!/bin/bash


die(){
        echo "Error: $1" >/dev/stderr
        exit 1
}

[ -z "$(which jq)" ] && die "utility jq is not available"

require_env(){
        var=$1
        [ ! -z "${!var}" ] || die "missing variable '$1' ($2)."
}

# read yaml file $1, jq path $2, to env variable $3
yaml_to_env(){
  value=$(cat "$1" | yaml_to_json | jq -M "$2" )
  varname=$3
  if [ "$value" == "null" ]; then
    echo "Cannot read value $2 from file $1" >/dev/stderr
    return 1
  fi
  rawvalue=$(echo "$value" | sed 's/^"//' | sed 's/"$//' )
  eval "$varname=\$rawvalue"
}


# { k: v str } => {v : k}
# reverse the keys and values in a dict. Obviously, if there are duplicate 
# values, some will get lost. 
JQ_REVERSE_DICT="to_entries | map( { value : .key } )| from_entries"

# {k:v} => {k:v}
# identity operator, but error if _KEY_ is not set
JQ_CHECK_HASKEY='. | if (._KEY_==null) then error("missing key [\"_KEY_\"] in dictionary") else . end'

# { select: [ str ] , from: {k:T} } => {k:T}
# contains only the keys mentioned in the .select list
JQ_SELECT_KEYS_FROM=". 
	| ${JQ_CHECK_HASKEY//_KEY_/select} 
	| ${JQ_CHECK_HASKEY//_KEY_/from} 
	| .select as \$select 
	| .from as \$from 
	| .from 
	| to_entries 
	| [ .[] 
	| .key as \$key 
	| select ( \$select | contains( [ \$key ] ) ) ] | from_entries "



# {k:[e str]} => {e: [ k ] }
# invert invert key/value pairs
# and create a dict with group entries containing lists of the original keys
JQ_INVERT_LISTDICT='
	to_entries 		# create a list of {key: str, value: [e]} dicts 
	
	| map({ key: .key, value: .value[] })   # flatten such that keys are duplicated and values are e elements
	| group_by(.value) 
	| [ 
		.[] 
		| { key: map( .key ), value: .[0].value}  
		| { key: .value, value: .key } 
	] 
	| from_entries '

# { k: [T] } => { K: { '_SUBKEY_': [T] }}
JQ_DICT_SUBDICT='to_entries  | map( { key : .key , value: { "_SUBKEY_": .value } } ) | from_entries'


reverse_dict(){
	jq -M "$JQ_REVERSE_DICT"
}

merge_two_json_files(){
        (
                echo "["; cat $1 | yaml_to_json;
                echo ","; cat $2 | yaml_to_json;
                echo "]";
        ) | jq -M 'reduce .[] as $item ({}; . * $item)'
}



#jq -M 'to_entries | map({ host: .key, group: .value | splits(",") })| group_by(.group) | [ .[] | { children: map( .host ), group: .[0].group} | { key: .group, value: .children } ] | from_entries '



select_keys_from() {
	jq -M $JQ_SELECT_KEYS_FROM
}


# select given key $1 from json dict on stdin
# the result should be a dict.
# merge with empty dict, i.e. return an empty dict if not found
select_from_dict(){
        yaml_to_json | jq -M ".[\"$1\"] + {}"
}



# convert the infraspec to a group list 
# Infraspec is: 
#          desired: 
#          - dev-nerdrouter-1
#          - dev-knode-01-1
#          hosts:
#            dev-nerdrouter-1: 
#              groups: [ nerdrouters ] 
#            dev-knode-01-1: 
#              groups: [ knodes, testservers] 
#            dev-knode-01-2: 
#              groups: [ knodes ] 
# Group list is of form: 
# 
#           knodes:
#           - dev-knode-01-1 # dev-knode-01-2 was not in 'desired'
#           testservers:
#           - dev-knode-01-1
#           nerdrouters:
#           - dev-nerdrouter-1
JQ_INFRASPEC_TO_INVENTRY__HOSTS_BY_GROUP=". 
	| { select: .desired | to_entries | map(.value) | flatten , from: .hosts}
	| $JQ_SELECT_KEYS_FROM
	
	# project group field
	# {k: {'groups':[str]}} => {k: [str]}
	| to_entries
	| map( { key : .key , value: .value.groups  } )  
	| from_entries
	
	| $JQ_INVERT_LISTDICT
	
	# move the list of nodes into a dict with ansible-playbook-json 'children' key
	# {k: [str]} =>  {k: {'hosts':[str]}} 
	| to_entries
	| map( { key : .key , value: { hosts:  .value  }} )  
	| from_entries
	"
	

TRANFORM=$JQ_INFRASPEC_TO_INVENTRY__HOSTS_BY_GROUP


# template bob variables in json text
# use bash-like variables, but with ${} to properly recognize the end. 
template_bob(){
	require_env DEPLOY_ENV "The variable specifying your deployment environment (dev/prod/acc)"
        sed "s/\${DEPLOY_ENV}/${DEPLOY_ENV}/g"
}

run_bob_subcommand(){
	case $1 in
	        --help|-*) # subcommands cannot start with -
	                help
	        ;;
	        *)
	                [ "$(type -t $1)" == "function" ] || help
	                $@ # run subcommand as function
	                ;;
	esac
}


