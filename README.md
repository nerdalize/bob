# Bob

## Goal
Attempt to make defining VM infrastructures simple and shareable. 

Vagrant helps you to define infrastructure, but it's config is different for 
VMWare/VirtualBox. This is unavoidable, as they have a different featureset.

Bob allows you to define an infrastructure (set of VM's and their network topology) 
in a yaml file. It allows you to define their group hierarchy in another yaml file. 
Furthermore, plugins allow you to create that infrastructure on localhost with 
Vagrant+VirtualBox, with Vagrant+VMWare, or via ssh on a remote host -- with any of the 
plugins available there. 

## Using bob

### Install bob

install dependencies:

* apt-get install python3-pip
* pip3 install peru


install bob: 

* `make install` 
* `export BOB_LIB=/usr/lib/bob`

### Get started

If you have virtualbox installed, you can: 

* `cd examples`
* `bob --help`
* `bob infra status` 
* `bob infra edit` 
* `bob infra up`

See the `example/` dir. You require 3 things: 

* a bob.yml file defining some project variables
* a bob_hosts file (path defined in bob.yml), specifying the hosts and their topology

With `bob inventory --list` and `bob inventory --host myhost` you get an ansible inventory. 
With `bob ssh myhost` you should be able to ssh to your host. 

Use the ansible-inventory file in the examples/ dir as your ansible dynamic inventory. It respects
the group structure of your bob_hosts file. 

### Host file
See the example hosts.yml. It specifies the hosts in the `enabled` dict, per group. You can quickly comment out hosts to not use them. 


The `hosts` dict specifies host details per host name. 
Fields are: 

* cores: the # of vcpu's
* memory: vm memory in mb
* ethernet: list of `{network: "network_name", mac: "de:ad:be:ef:00:01"}` dicts; in this case `"network_name"` must be entry in networks section
* forwarded_ports: list of port mappings expressed in "host:guestport" string  (virtualbox only)

Section `networks` dict specifies the details of every network. The simplest spec is an empty dict. Options are: 

* dhcp: a dict `{ip: , netmask: , lower: , upper: }` specifying the dhcp to run on this network (virtualbox only). 


Section `groups` specifies a group hierarchy. It doesn't specify any hosts itself -- those are in the enabled section. The direct parent groups of the hosts in the `enabled` section are folded into the group hierarchy. 

## Plugins

### vagrant-topology
Should work for Vmware Workstation and VirtualBox. Differences between the working is that VirtualBox will create the VM's eth0 interfaces behind NAT, but invisible for the guest. It uses port-forwarding, which means you can ssh to e.g. 127.0.0.1:2206 to reach the host. This means that if you want to access a webserver, you need to create a "hostport:guestport" entry in the `hosts.HOSTNAME.forwarded_ports` list of your hosts file. 

### bob-remote
Spawn infra on a remote bob project (which is in turn using vagrant-topology method). Works via ssh as configured in bob.yml. Only tricky thing is that it uses the remote bob.yml definition for hosts and group yml files. 

### implementing your own
Implement an infra plugin by implementing the bob methods in a `infrastructure/pluginname/bob-impl` file. See an existing plugin as example. 

## Limitations
Actually it's not possible to make this infra definition entirely uniform. Maybe it's a terribly bad
idea. Maybe we can use the common subset of GCE/AWS/VMWare/VirtualBox to make an abstraction. Maybe we went too far already. Who knows. 
