#!/bin/bash

# reformat terraform output variables grouped by variable in a format
# grouped by node.
#
# The Nice Thing (TM) is that the script is agnostic to the exact variable and nodenames
# that are output by Terraform. 
#

# terraform output -module=nodes is assumed to output something like: 
#
#  instance_id = { test-discovery-1: 'i-2a8c3d93' , test-discovery-2: 'i-bc4e5e05' , test-discovery-3: 'i-605ed7ed'  }
#  private_ip = { test-discovery-1: '10.10.1.223' , test-discovery-2: '10.10.2.233' , test-discovery-3: '10.10.3.231'    }
#  public_ip = { test-discovery-1: '54.229.167.55' , test-discovery-2: '54.229.141.254' , test-discovery-3: '54.229.155.56'   }
#
#
#
# the query will format it to:
#
#  {
#    "test-discovery-1": {
#      "public_ip": "54.229.167.55",
#      "instance_id": "i-2a8c3d93",
#      "private_ip": "10.10.1.223"
#    },
#    "test-discovery-2": {
#      "public_ip": "54.229.141.254",
#      "instance_id": "i-bc4e5e05",
#      "private_ip": "10.10.2.233"
#    },
#    "test-discovery-3": {
#      "public_ip": "54.229.155.56",
#      "instance_id": "i-605ed7ed",
#      "private_ip": "10.10.3.231"
#    }
#  }



# this QUERY is a bit complex and I wouldn't recommend hacking it; maybe redo it in python unless
# you are adventurous.
#
# -eric


QUERY=' to_entries | map ( .key as $varname | .value | to_entries | map ( { host: .key, key: $varname, value : .value  }  )    ) | flatten | group_by(.host) | map ({ key: .[1].host, value: (.| from_entries) } ) | from_entries ' 
terraform output -module=nodes | grep "^inventory__" | sed 's/^inventory__//' | sed 's/ = /:/ '  \
	| yaml_to_json \
        | jq -M " . + {} | $QUERY" # convert to {} if there is no output

