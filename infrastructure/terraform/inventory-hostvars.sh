#!/bin/bash


# this is a bit complex and I wouldn't recommend hacking it; maybe redo it in python
# -eric
. util.sh

[ -z "$1" ] && die "need a hostname"

QUERY=".[\"$1\"]  | . + { ansible_ssh_host: .public_ip, ansible_ssh_port: 22 } "


jq -M "$QUERY" node-vars

