LIBDIR=/usr/local/lib/bob

help:
	@echo "only target is install"


install:
	cp bob /usr/local/bin/bob && \
	mkdir -p $(LIBDIR) && \
	cp -r * $(LIBDIR) && \
	cp lib/yaml_to_json.py /usr/local/bin/yaml_to_json && \
	echo "\n\nOK: You must now set 'export BOB_LIB=$(LIBDIR)' in your ~/.bashrc"

